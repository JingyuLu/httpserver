package org.whl.server;

import java.io.IOException;
import java.net.ServerSocket;

import org.whl.tools.CloseStream;

public class Server {
	
	private ServerSocket server;
	public static final String CRLF="\r\n";
	public static final String BLANK=" ";

	private boolean isShutdown=false;
	
	public static void main(String[] args) {

		Server myServer=new Server();
		myServer.start();
	}

	/**
	 * start server
	 */
	public void start() {
		start(8888);
	}
	
	public void start(int port) {
		try {
			server=new ServerSocket(port);
			this.receive();
		} catch (IOException e) {
			stop();
		}
	}
	
	/**
	 * receive info from client
	 */
	private void receive() {
		try {
			while(!isShutdown) {
				new Thread(new Dispatcher(server.accept())).start();
			}
		} catch (IOException e) {
			stop();
		}
	}
	
	/**
	 * stop server
	 */
	public void stop() {
		isShutdown=true;
		CloseStream.closeAll(server);
	}
}
