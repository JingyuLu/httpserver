package org.whl.server;

import java.io.IOException;
import java.net.Socket;

import org.whl.servlet.Servlet;
import org.whl.tools.CloseStream;

public class Dispatcher implements Runnable {

	private Socket client;
	private Request req;
	private Response rep;
	private int code = 200;

	public Dispatcher(Socket client) {
		this.client = client;
		try {
			req = new Request(client.getInputStream());
			rep = new Response(client.getOutputStream());
		} catch (IOException e) {
			code = 500;
			return;
		}

	}

	@Override
	public void run() {
		try {
			Servlet servlet = WebApp.getServlet(req.getUrl());
			if (servlet == null) {
				this.code = 404;
			} else {
				servlet.service(req, rep);
			}

			rep.pushDataToClient(code);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			this.code = 500;
		}
		try {
			rep.pushDataToClient(500);
		} catch (IOException e) {
			e.printStackTrace();
		}
		CloseStream.closeAll(client);
	}

}
