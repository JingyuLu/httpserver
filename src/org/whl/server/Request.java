package org.whl.server;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

public class Request {

	private String method;
	private String url;
	private Map<String, List<String>> paraValues;
	private String requestInfo;
	private InputStream is;

	public static final String CRLF = "\r\n";

	public Request() {
		method = "";
		url = "";
		paraValues = new HashMap<String, List<String>>();
		requestInfo = "";
	}

	public Request(InputStream is) {
		this();
		this.is = is;
		try {
			byte[] data = new byte[20480];
			int len = is.read(data);
			requestInfo = new String(data, 0, len);
		} catch (IOException e) {
			return;
		}
		parseRequestInfo();
	}

	public String getUrl() {
		return url;
	}

	private void parseRequestInfo() {

		if (requestInfo == null || requestInfo.trim().equals("")) {
			return;
		}

		String paramString = "";

		String firstLine = requestInfo.substring(0, requestInfo.indexOf(CRLF));
		int methodIndex = requestInfo.indexOf("/");
		String urlStrs = firstLine.substring(methodIndex, firstLine.indexOf("HTTP/")).trim();
		this.method = firstLine.substring(0, methodIndex).trim();
		if (this.method.equalsIgnoreCase("post")) {
			this.url = urlStrs;
			paramString = requestInfo.substring(requestInfo.lastIndexOf(CRLF)).trim();
		} else if (this.method.equalsIgnoreCase("get")) {
			if (urlStrs.contains("?")) {
				String[] tmp = urlStrs.split("\\?");
				this.url = tmp[0];
				paramString = tmp[1];
			} else {
				this.url = urlStrs;
			}
		}

		if (paramString.equals("")) {
			return;
		}
		parseParams(paramString);
	}

	private void parseParams(String paramString) {
		StringTokenizer token = new StringTokenizer(paramString, "&");
		while (token.hasMoreTokens()) {
			String keyValueToken = token.nextToken();
			String[] keyValues = keyValueToken.split("=");
			if (keyValues.length == 1) {
				keyValues = Arrays.copyOf(keyValues, 2);
				keyValues[1] = null;
			}

			String key = keyValues[0].trim();
			String value = keyValues[1] == null ? null : decode(keyValues[1].trim(), "GBK");

			if (!paraValues.containsKey(key)) {
				paraValues.put(key, new ArrayList<String>());
			}
			List<String> values = paraValues.get(key);
			values.add(value);
		}
	}

	/**
	 * get the value of submission by name
	 */
	public String getParameter(String name) {
		String[] values = getParameterValues(name);
		if (values == null) {
			return null;
		}
		return values[0];
	}

	/**
	 * get the values of submission by name
	 */
	public String[] getParameterValues(String name) {
		List<String> values = null;
		if ((values = paraValues.get(name)) == null) {
			return null;
		} else {
			return values.toArray(new String[0]);
		}
	}

	/**
	 * Chinese encoding problem
	 */
	private String decode(String value, String code) {

		try {
			return URLDecoder.decode(value, code);
		} catch (UnsupportedEncodingException e) {
		}
		return null;
	}

}
