package org.whl.server;

import java.util.List;
import java.util.Map;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.whl.servlet.Servlet;

public class WebApp {

	private static ServletContext context;

	static {

		try {
			SAXParserFactory factory = SAXParserFactory.newInstance();
			SAXParser sax = factory.newSAXParser();
			WebHandler web = new WebHandler();
			sax.parse(Thread.currentThread().getContextClassLoader().getResourceAsStream("WEB_INFO/web.xml"), web);

			context = new ServletContext();

			// servlet-name servlet-class
			Map<String, String> servlet = context.getServlet();
			for (Entity entity : web.getEntityList()) {
				servlet.put(entity.getName(), entity.getClz());
			}

			// url-pattern servlet-name
			Map<String, String> mapping = context.getMapping();
			for (Mapping map : web.getMappingList()) {
				List<String> urls = map.getUrlPattern();
				for (String url : urls) {
					mapping.put(url, map.getName());
				}

			}

		} catch (Exception e) {
		}

	}

	public static Servlet getServlet(String url)
			throws InstantiationException, IllegalAccessException, ClassNotFoundException {
		if (url == null || url.trim().equals("")) {
			return null;
		}

		String name = context.getServlet().get(context.getMapping().get(url));
		return (Servlet) Class.forName(name).newInstance();

	}
}
