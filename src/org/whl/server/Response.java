package org.whl.server;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.util.Date;

import org.whl.tools.CloseStream;

public class Response {

	private StringBuilder headInfo;
	private int len = 0;
	StringBuilder content;
	private BufferedWriter bw;

	public static final String CRLF = "\r\n";
	public static final String BLANK = " ";

	public Response() {
		headInfo = new StringBuilder();
		content = new StringBuilder();
		len = 0;
	}

	public Response(Socket client) {
		this();
		try {
			bw = new BufferedWriter(new OutputStreamWriter(client.getOutputStream()));
		} catch (IOException e) {
			headInfo = null;
		}
	}

	public Response(OutputStream os) {
		this();
		bw = new BufferedWriter(new OutputStreamWriter(os));
	}

	/**
	 * create content
	 */
	public Response buildContent(String info) {
		content.append(info);
		len += info.getBytes().length;
		return this;
	}

	/**
	 * create contents
	 */
	public Response buildMultiRowContent(String info) {
		content.append(info).append(CRLF);
		len += (info + CRLF).getBytes().length;
		return this;
	}

	/**
	 * response head
	 */
	private void createHeadInfo(int code) {
		if (headInfo == null) {
			code = 500;
		}
		// Http version + status code & description
		headInfo.append("HTTP/1.1").append(BLANK).append(code).append(BLANK);
		switch (code) {
		case 200:
			headInfo.append("OK");
			break;
		case 404:
			headInfo.append("Not Found");
			break;
		case 500:
			headInfo.append("Server Error");
			break;
		}
		headInfo.append(CRLF);

		// Response Head
		headInfo.append("Server:WHL Test Server/666").append(CRLF); // server info
		headInfo.append("Date:").append(new Date()).append(CRLF); // date
		headInfo.append("Content-type:text/html;charset=GBK").append(CRLF); // type & charset
		headInfo.append("Content-Length:").append(len).append(CRLF); // *length

		headInfo.append(CRLF);
	}

	void pushDataToClient(int code) throws IOException {
		createHeadInfo(code);
		bw.append(headInfo.toString());
		bw.append(content.toString());
		bw.flush();
	}

	public void close() {
		CloseStream.closeAll(bw);
	}
}
