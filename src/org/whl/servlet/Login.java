package org.whl.servlet;

import org.whl.server.Request;
import org.whl.server.Response;

public class Login extends Servlet {

	@Override
	public void doGet(Request req, Response rep) throws Exception {

		String name = req.getParameter("uname");
		String pwd = req.getParameter("pwd");
		if (login(name, pwd)) {
			rep.buildMultiRowContent("Login Success");
		} else {
			rep.buildMultiRowContent("Login Failed");
		}
	}

	@Override
	public void doPost(Request req, Response rep) throws Exception {

	}

	public boolean login(String name, String pwd) {
		return name.equals("whl") && pwd.equals("666");
	}
}
