package org.whl.tools;

import java.io.Closeable;
import java.io.IOException;

public class CloseStream {
	public static void closeAll(Closeable... io) {
		for (Closeable tmp : io) {
			try {
				if (tmp != null) {
					tmp.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
